# About #

This module is intended to help site administrators apply rules to replace broken links that are found with the linkchecker module when manual interventions would be too arduous.

This module comes with two rules (disabled by default): replace with base URL (rewrite example.com/my/page to example.com), and replace with fixed URL (to allow you to create a page explaining broken links).

The links are replaced in blocks, nodes, and comments as discovered and repaired by the linkchecker module's internal functions.

# Installation #

  * Get an install required modules: (linkchecker)[1]
  * Unpack into sites/all/modules or sites/example.com/modules
  * Enable through the drupal interface or with drush (drush en link_repairer)

# Configuration #

  * Go to admin/config/content/linkchecker
  * Enable necessary rules, set options (if any), and threshold in the Error Handling -> Link Repairer section
  * Save

[1]: https://www.drupal.org/project/linkchecker
