<?php

/**
 * @file
 * API documentation for link_repairer module.
 */

/**
 * Defines a new repair rule.
 *
 * @returns array
 *   Index is the repair rule id associated with an array of configuration
 *   options.
 */
function callback_link_repairer_rules() {
  return array(
    'example_rewrite_rule' => array(
      'title' => t('Human readable title'),
      'description' => t('Human readable description'),
      'callback' => '_mycallback_function',
      //'file' => '.repairer_rules.inc', // optional
      //'file path' => drupal_get_module('my module'), //optional
    ),
  );
}

/**
 * Alters repairer rule definitions.
 *
 * Modify rule declarations after they are collected initially.
 */
function callback_link_repairer_rules_alter(&$rules) {
  if ($rules['example_rewrite_rule']) {
    $rules['example_rewrite_rule']['callback'] = '_some_other_callback';
  }
}

/**
 * React to updating link in a node.
 *
 * @param $node object
 *   A clone of the node object that is about to be altered.
 *
 * @param $context array
 *   Array containing contextual information on the operation:
 *     'rule' => (array) rule definition
 *     'link' => (object) the link object that was loaded from the db
 *     'url_options' => (array) different url combinations that might be tried
 *     'new_url' => (string) the url returned by the link_repairer rule callback
 *     'perform_default_action' => (boolean) TRUE by default. Set to FALSE to
 *       avoid running the default action (_linkchecker_replace_fields).
 *
 */
function callback_link_repairer_node_link_alter(&$node, &$context) {
  if ($context['rule']['machine_name'] == 'some_rule') {
    // Make some change
    // Don't call default action.
    $context['call_replace_fields'] = FALSE;
  }
}
